print( "Running bxb scripts" );

### bxb/crashes

#recipes.remove( <extrautils2:opinium:*> ); # fixed in 1.2.0
#<extrautils2:opinium:*>.addTooltip( format.darkRed( "Banned for causing crashes." ) );

#recipes.remove(<extrautils2:klein>); # fixed in 1.2.0

#recipes.remove(<chisel:chisel_hitech>); # handled by ftb in normal.zs

### bxb/dupes

#recipes.remove( <thermalfoundation:wrench> ); # fixed in 1.1.0
#<thermalfoundation:wrench>.addTooltip( format.darkRed( "Banned for item dupes." ) );

# bxb/??? ask link

recipes.remove(<draconicevolution:celestial_manipulator>);
recipes.remove(<opencomputers:case1>);
recipes.remove(<opencomputers:case2>);
recipes.remove(<opencomputers:case3>);
recipes.remove(<opencomputers:material:*>);
recipes.remove(<refinedstorage:crafter>); # may be fixed
recipes.remove(<tconstruct:throwball:1>); # make and move to op

